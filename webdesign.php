<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MiMove</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800">
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/keyframes.css">
    <link rel="stylesheet" href="assets/css/layout.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/pageTransitions.css">
    <link rel="stylesheet" href="assets/css/expandable-image-gallery.css">
    <link rel="stylesheet" href="https://cdn.knightlab.com/libs/juxtapose/latest/css/juxtapose.css">
  </head>
  <body>
    <div class="detail m-scene" id="main">
      <div class="m-detail-layout webdesign">
        <?php include("inc/primary-menu.php"); ?>

        <div class="m-right-panel m-page scene_element scene_element--fadein">
          <div class="right-panel_top m-webdesign">
            <div class="m-header">
              <div class="m-breadcrumb" itemprop="breadcrumb">
                <h1 class="m-type-display-1">
                  Webbdesign
                </h1>
                <p class="m-type-sub-heading-1">To run this demo locally, start a server in the root of this repository. If you have python installed, you can simply run <code>python -m SimpleHTTPServer</code> after you <code>cd</code> into the repo.</p>
              </div>

            </div>
          </div>
          <div class="m-segment m-webdesign">

            <h2 class="m-type-heading-1">Fjelkners - Moderns mäklardesign i traditionell bransch</h2>

            <img src="assets/img/misc-fjelkners-2.jpg" data-0="opacity: 1; left: 2%; transform: scale(1);" data-250="opacity: 1;" data-500="opacity: 0.2; left: 5%; transform: scale(1);" class="fjelkners-1" style="max-width: 100%;" alt="Product Image 1">

            <img src="assets/img/misc-fjelkners.jpg" style="max-width: 100%;" alt="Product Image 1">

            <section class="cd-content">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum voluptatem, quisquam veniam sequi in quasi excepturi laudantium fugit nihil odio minima quae consequuntur dolorum pariatur obcaecati, adipisci dignissimos officia saepe itaque deleniti porro odit vitae voluptate. Blanditiis sunt obcaecati corporis, alias adipisci. Eum illum voluptatibus expedita nulla eius provident pariatur!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum voluptatem, quisquam veniam sequi in quasi excepturi laudantium fugit nihil odio minima quae consequuntur dolorum pariatur obcaecati, adipisci dignissimos officia saepe itaque deleniti porro odit vitae voluptate.
              </p>
            </section>

            <div class="juxtapose" data-showlabels="false" data-showcredits="false">
              <img src="assets/img/misc-distit-wireframe.png" data-label="2009" data-credit="Alex Duner/Northwestern Knight Lab" />
              <img src="assets/img/misc-distit.png" data-label="2009" data-credit="Alex Duner/Northwestern Knight Lab" />
            </div>

            <h2 class="m-type-heading-1">Fjelkners - Moderns mäklardesign i traditionell bransch</h2>
            <img src="assets/img/mobil-design.png" style="max-width: 100%;" alt="Product Image 1">

            <div class="segment_content">
              <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
              <ul>
                <li>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</li>
                <li>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</li>
                <li>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</li>
                <li>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.</li>
              </ul>
              <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
            </div>
          </div>
          <div class="m-segment m-webdesign">
            <h2 class="m-type-heading-1">sit minus cum porro ipsum et fugiat totam</h2>
            <div class="segment_content">
              <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
              <dl>
                <dt>Definition list</dt>
                <dd>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                  aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                  commodo consequat.
                </dd>
                <dt>Lorem ipsum dolor sit amet</dt>
                <dd>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                  aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                  commodo consequat.
                </dd>
              </dl>
              <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
            </div>
          </div>

          <?php include("inc/footer.php"); ?>
        </div>
      </div>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="assets/js/jquery.mobile.min.js"></script>
    <script src="assets/js/jquery.smoothState.min.js"></script>
    <script src="assets/js/main.js"></script>
  </body>
</html>
