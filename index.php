<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#000000" />
    <title>Home - jQuery.smoothState.js</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,800">
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/keyframes.css">
    <link rel="stylesheet" href="assets/css/layout.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/pageTransitions.css">
    <link rel="stylesheet" href="assets/css/expandable-image-gallery.css">
    <!-- <link rel="stylesheet" href="https://cdn.knightlab.com/libs/juxtapose/latest/css/juxtapose.css"> -->
  </head>
  <body>
    <div class="index m-scene" id="main">
      <div class="m-landing-layout">
        <div class="m-header scene_element scene_element--fadein">
          <p class="m-type-sub-heading-1 header_intro">KRISTOFFER BERGLUND</p>
          <h1 class="m-type-display-1">Case<br />Studies</h1>
          <p class="m-type-sub-heading-1 header_intro">2014-2016</p>
          <p class="m-type-sub-heading-1 header_intro"><img src="assets/img/linkedin.svg" alt="Go to my LinkedIn profile" title="Go to my LinkedIn profile" style="width: 46px;"/></p>
        </div>
        <div class="m-page">
          <div class="m-segment m-mimove scene_element scene_element--fadeinup delay-80">
            <a href="./mimove-new.php">
              <h2 class="m-type-heading-1">
                <!-- <img src="assets/img/logo-mimove-white.svg" alt="MiMove" title="MiMove" />
                <span>MiMove</span> -->
                MiMove
              </h2>
              <p class="description">Creating a graphic identity for a international brand</span></p>
              <button class="btn btn-sm btn-filled">>></button>
            </a>
          </div>
          <div class="m-segment m-fasad scene_element scene_element--fadeinup delay-140">
            <a href="./fasad-new.php">
              <h2 class="m-type-heading-1">
                <!-- <img src="assets/img/FasAd-logo-white.svg" alt="FasAd" title="FasAd" />
                <span>FasAd</span> -->
                FasAd
              </h2>
              <p class="description">Modernizing a flag-ship product</span></p>
              <button class="btn btn-sm btn-filled">>></button>
            </a>
          </div>
          <div class="m-segment m-prek scene_element scene_element--fadeinup delay-200">
            <!-- <a href="./prek.php">
              <h2 class="m-type-heading-1"><img src="assets/img/Prek_vektor_black.svg" alt="Prek" title="Prek" /><span>Prek</span></h2>
              <p class="description">Logo revamp for company in Sweden</span></p>
              <button class="btn btn-sm btn-filled">>></button>
            </a> -->
            <a href="./design-new.php">
              <h2 class="m-type-heading-1">Design</h2>
              <p class="description">Design tailored to real estate market</span></p>
              <button class="btn btn-sm btn-filled">>></button>
            </a>
          </div>
          <!-- <div class="m-segment m-webdesign scene_element scene_element--fadeinup delay-300">
            <div class="segment_content">
              <a href="./webdesign.php">
                <h2 class="m-type-heading-1">Design
                </h2>
                <p class="description">Design tailored to real estate market</span></p>
                <button class="btn btn-sm btn-filled">>></button>
              </a>
            </div>
          </div> -->
        </div>
      </div>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="assets/js/jquery.smoothState.min.js"></script>

  </body>
</html>
