<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="theme-color" content="#000000" />
    <title>MiMove</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Libre+Franklin|Open+Sans+Condensed:300,700|Roboto+Slab:400,700" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/keyframes.css">
    <link rel="stylesheet" href="assets/css/layout.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/pageTransitions.css">
    <!-- <link rel="stylesheet" href="assets/css/expandable-image-gallery.css"> -->
    <!-- <link rel="stylesheet" href="https://cdn.knightlab.com/libs/juxtapose/latest/css/juxtapose.css"> -->
  </head>
  <body>
    <div class="detail m-scene" id="main">
      <div class="m-detail-layout mimove">
        <?php include("inc/primary-menu.php"); ?>

        <div class="m-right-panel m-page scene_element scene_element--fadein">
          <div class="right-panel_top m-mimove">
            <div class="m-header">
              <div class="m-breadcrumb" itemprop="breadcrumb">
                <h1 class="m-type-display-1">Design</h1>
                <p class="m-type-sub-heading-1">Creating a game changing platform for international appeal. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
              </div>
            </div>
          </div>

          <div class="m-segment">
            <!-- <h2 class="m-type-heading-1">aut a voluptas ipsa voluptatibus repellendus</h2> -->


                          <img src="assets/img/mimove-design-1-cut.png" class="img-responsive" alt="Product Image 1">

                          <h2>An advanced CRM system</h2>
                          <p>Adopting material design principles made it easy for the developers to adhere to specific guidelines. Made it so that developers were able to make own decisions about layout and design by following the material design principles and guidelines. Made for some fun discussions in the group on user behaviour and functionalty.</p>

                          <p>Key decision was to adopt material design principles following a difficult previous project.
                          Following a diffucult previous project my key decision was to adopt material design principles.</p>
                          <button class="btn btn-m btn-filled">Test-drive prototype</button>

          </div>

          <?php include("inc/footer.php"); ?>
        </div>
      </div>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <!-- <script src="assets/js/jquery.mobile.min.js"></script> -->
    <script src="assets/js/jquery.smoothState.min.js"></script>
    <!-- <script src="assets/js/main.js"></script> -->
    <script src="assets/js/main-test.js"></script>
  </body>
</html>
