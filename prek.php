<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MiMove</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Libre+Franklin:400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/keyframes.css">
    <link rel="stylesheet" href="assets/css/layout.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/pageTransitions.css">
    <link rel="stylesheet" href="assets/css/expandable-image-gallery.css">
    <link rel="stylesheet" href="https://cdn.knightlab.com/libs/juxtapose/latest/css/juxtapose.css">
  </head>
  <body>
    <div class="detail m-scene" id="main">
      <div class="m-detail-layout prek">
        <?php include("inc/primary-menu.php"); ?>

        <div class="m-right-panel m-page scene_element scene_element--fadein">
          <div class="right-panel_top m-prek">
            <div class="m-header">
              <div class="m-breadcrumb" itemprop="breadcrumb">
                <h1 class="m-type-display-1">
                  <img src="assets/img/Prek_vektor_black.svg" alt="Prek" title="Prek" />
                  <span>Prek</span>
                </h1>
                <p class="m-type-sub-heading-1">To run this demo locally, start a server in the root of this repository. If you have python installed, you can simply run <code>python -m SimpleHTTPServer</code> after you <code>cd</code> into the repo.</p>
              </div>
            </div>
          </div>
          <div class="m-segment full-width">
            <h2 class="m-type-heading-1">aut a voluptas ipsa voluptatibus repellendus</h2>

            <section class="cd-single-item">
              <div class="cd-slider-wrapper">
                <ul class="cd-slider">
                  <li class="selected"><img src="assets/img/mimove-design-1.png" alt="Product Image 1"></li>
                  <li><img src="assets/img/img-2.jpg" alt="Product Image 1"></li>
                  <li><img src="assets/img/img-3.jpg" alt="Product Image 2"></li>
                </ul> <!-- cd-slider -->

                <ul class="cd-slider-navigation">
                  <li><a href="#0" class="cd-prev inactive">Next</a></li>
                  <li><a href="#0" class="cd-next">Prev</a></li>
                </ul> <!-- cd-slider-navigation -->

                <a href="#0" class="cd-close">Close</a>
              </div> <!-- cd-slider-wrapper -->

              <div class="cd-item-info">
                <h2>Produt Title</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia, omnis illo iste ratione.</p>

                <button class="btn btn-m">Add to cart</button>
              </div> <!-- cd-item-info -->
            </section> <!-- cd-single-item -->
          </div>

          <div class="m-segment">
            <section>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum voluptatem, quisquam veniam sequi in quasi excepturi laudantium fugit nihil odio minima quae consequuntur dolorum pariatur obcaecati, adipisci dignissimos officia saepe itaque deleniti porro odit vitae voluptate. Blanditiis sunt obcaecati corporis, alias adipisci. Eum illum voluptatibus expedita nulla eius provident pariatur!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum voluptatem, quisquam veniam sequi in quasi excepturi laudantium fugit nihil odio minima quae consequuntur dolorum pariatur obcaecati, adipisci dignissimos officia saepe itaque deleniti porro odit vitae voluptate.
              </p>
            </section>
          </div>

          <div class="m-segment">
            <div class="juxtapose" data-showlabels="false" data-showcredits="false">
              <img src="assets/img/fasad-3.png" data-label="2009" data-credit="Alex Duner/Northwestern Knight Lab" />
              <img src="assets/img/fasad-3.png" data-label="2009" data-credit="Alex Duner/Northwestern Knight Lab" />
            </div>
          </div>

          <div class="m-segment">
            <section>
              <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
              <ul>
                <li>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</li>
                <li>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</li>
                <li>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</li>
                <li>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.</li>
              </ul>
              <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
            </section>
          </div>

          <div class="m-segment article">
            <h2 class="m-type-heading-1 text-center">sit minus cum porro ipsum et fugiat totam</h2>
            <section class="three-columns">
              <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
              <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
            </section>
          </div>

          <?php include("inc/footer.php"); ?>
        </div>
      </div>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="assets/js/jquery.mobile.min.js"></script>
    <script src="assets/js/jquery.smoothState.min.js"></script>
    <script src="assets/js/main.js"></script>
  </body>
</html>
