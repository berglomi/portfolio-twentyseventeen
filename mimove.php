<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MiMove</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Libre+Franklin|Open+Sans+Condensed:300,700|Roboto+Slab:400,700" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="assets/css/keyframes.css">
    <link rel="stylesheet" href="assets/css/layout.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/pageTransitions.css">
    <link rel="stylesheet" href="assets/css/expandable-image-gallery.css">
    <!-- <link rel="stylesheet" href="https://cdn.knightlab.com/libs/juxtapose/latest/css/juxtapose.css"> -->
  </head>
  <body>
    <div class="detail m-scene" id="main">
      <div class="m-detail-layout mimove">
        <?php include("inc/primary-menu.php"); ?>

        <div class="m-right-panel m-page scene_element scene_element--fadein">
          <div class="right-panel_top m-mimove">
            <div class="m-header">
              <div class="m-breadcrumb" itemprop="breadcrumb">
                <h1 class="m-type-display-1">
                  <img src="assets/img/logo-mimove-black.svg" alt="MiMove" title="MiMove" />
                  <span>MiMove</span>
                </h1>
                <p class="m-type-sub-heading-1">Creating a game changing platform for international appeal. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p>
              </div>
            </div>
          </div>

          <div class="m-segment full-width">
            <!-- <h2 class="m-type-heading-1">aut a voluptas ipsa voluptatibus repellendus</h2> -->

            <section class="cd-single-item">
              <div class="cd-slider-wrapper" style="transition: all 100ms;">
              <!-- <div class="cd-slider-wrapper" style="transition: all 100ms;" data-0="opacity: 0.4; left: 40px; top: 0; transform: skew(10deg,10deg) scale(0.8);" data-500="opacity: 1; left: 0; top: 20px; transform: skew(0deg,0deg) scale(1);"> -->
                <ul class="cd-slider scene_element">
                  <li class="selected"><img src="assets/img/mimove-design-1-cut.png" alt="Product Image 1"></li>
                  <li><img src="assets/img/img-2.jpg" alt="Product Image 1"></li>
                  <li><img src="assets/img/img-3.jpg" alt="Product Image 2"></li>
                </ul> <!-- cd-slider -->

                <ul class="cd-slider-navigation">
                  <li><a href="#0" class="cd-prev inactive">Next</a></li>
                  <li><a href="#0" class="cd-next">Prev</a></li>
                </ul> <!-- cd-slider-navigation -->

                <a href="#0" class="cd-close">Close</a>

                <a href="#0" class="cd-open"><span class="pulsate-animation"></span></a>
              </div> <!-- cd-slider-wrapper -->

              <div class="cd-item-info">
              <!-- <div class="cd-item-info" data-0="opacity: 0.1; padding-top: -40px; padding-left: 80px;" data-250="opacity: 1; padding-top: 80px; padding-left: 120px;"> -->
                <h2>An advanced CRM system</h2>
                <p>Adopting material design principles made it easy for the developers to adhere to specific guidelines. Made it so that developers were able to make own decisions about layout and design by following the material design principles and guidelines. Made for some fun discussions in the group on user behaviour and functionalty.</p>

                <p>Key decision was to adopt material design principles following a difficult previous project.
                Following a diffucult previous project my key decision was to adopt material design principles.</p>
                <button class="btn btn-m btn-filled">Test-drive prototype</button>
              </div> <!-- cd-item-info -->
            </section> <!-- cd-single-item -->

          </div>

          <div class="m-segment" data-0="opacity: 0.4;" data-500="opacity: 1;">
            <section>
              <h2 class="m-type-heading-1 text-center">Material design principles</h2>
              <p>
                Adopting material design principles made it easy for the developers to adhere to specific guidelines. Made it so that developers were able to make own decisions about layout and design by following the material design principles and guidelines. Made for some fun discussions in the group on user behaviour and functionalty. Key decision was to adopt material design principles following a difficult previous project.
                Following a diffucult previous project my key decision was to adopt material design principles.
              </p>
            </section>
          </div>

          <div class="m-segment" data-0="opacity: 0.4;" data-500="opacity: 1;">
            <h2 class="m-type-heading-1 text-center">From 0 to 100. Wireframe and design evolution</h2>
            <div class="juxtapose" data-showlabels="false" data-showcredits="false">
              <img src="assets/img/mimove-compare-1.png" data-label="2009" data-credit="Alex Duner/Northwestern Knight Lab" />
              <img src="assets/img/mimove-compare-2.png" data-label="2009" data-credit="Alex Duner/Northwestern Knight Lab" />
            </div>
          </div>

          <div class="m-segment article three-columns" data-0="opacity: 0.4;" data-500="opacity: 1;">
            <section>
              <h2 class="m-type-heading-1 text-center">A real estate portal for sellers and buyers</h2>
              <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
            </section>
            <section>
              <h2 class="m-type-heading-1 text-center">Produced wireframes in Balsamiq Invision and other apps</h2>
              <p>Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
            </section>
            <section>
              <h2 class="m-type-heading-1 text-center">Video editing and after effects</h2>
              <p>Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus. </p>
              <button class="btn btn-sm btn-filled">Play video</button>
            </section>
          </div>

          <?php include("inc/footer.php"); ?>
        </div>
      </div>
    </div>

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="assets/js/jquery.mobile.min.js"></script>
    <script src="assets/js/jquery.smoothState.min.js"></script>
    <script src="assets/js/main.js"></script>
  </body>
</html>
