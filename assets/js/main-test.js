reInitScripts();

function reInitScripts() {
  // script from mail.js goes heretom

  // $.getScript('assets/js/juxtapose.min.js', function() {});
  $.getScript('assets/js/headroom.min.js', function() {});
  // $.getScript('assets/js/jquery.waypoints.min.js', function() {});
  // $.getScript('assets/js/inview.min.js', function() {});
  $.getScript('assets/js/skrollr.min.js', function() {});


  // Document ready
  jQuery(document).ready(function($){


    // Headroom

    // grab an element
    var myElement = document.querySelector(".headroom");
    // construct an instance of Headroom, passing the element
    var headroom  = new Headroom(myElement);
    // initialise
    headroom.init();

    // Skrollr
    // TODO
    // Find a way to initialise only on desktops.
    var s = skrollr.init({
        smoothScrolling: false
    });

  });

}

$(function(){
  'use strict';
  var $page = $('#main'),
      options = {
        debug: true,
        prefetch: true,
        cacheLength: 2,
        onStart: {
          duration: 250, // Duration of our animation
          render: function ($container) {
            // Add your CSS animation reversing class
            $container.addClass('is-exiting');
            // Restart your animation
            smoothState.restartCSSAnimations();
          }
        },
        onReady: {
          duration: 0,
          render: function ($container, $newContent) {
            // Remove your CSS animation reversing class
            $container.removeClass('is-exiting');
            // Inject the new content
            $container.html($newContent);
          }
        },
        onAfter: function() {
            reInitScripts();
        }
      },
      smoothState = $page.smoothState(options).data('smoothState');
});
