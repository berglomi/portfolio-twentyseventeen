<div class="m-segment">
  <h1 style="font-size: 40px;">Case studies</h1>
</div>
<nav id="secondary-menu">
  <div class="m-segment">
    <a href="./mimove-new.php" style="display: block;">
      <h2 class="m-type-heading-1">
        MiMove
      </h2>
      <p class="description">Creating a graphic identity for a international brand.</span></p>
    </a>
  </div>

  <div class="m-segment">
    <a href="./fasad-new.php" style="display: block;">
      <h2 class="m-type-heading-1">
        FasAd
      </h2>
      <p class="description">Modernizing a flag-ship product</span></p>
    </a>
  </div>

  <div class="m-segment">
    <a href="./design-new.php" style="display: block;">
      <h2 class="m-type-heading-1">
        Design
      </h2>
      <p class="description">Design tailored to real estate market</span></p>
    </a>
  </div>

  <!--
  <div class="m-segment">
    <div class="segment_content">
      <a href="mimove.php"><img src="assets/img/logo-mimove-white.svg" alt="MiMove" title="MiMove" /><span>MiMove</span></a>
    </div>
  </div>

  <div class="m-segment">
    <div class="segment_content">
      <a href="fasad.php"><img src="assets/img/FasAd-logo-white.svg" alt="FasAd" title="FasAd" /><span>FasAd</span></a>
    </div>
  </div>

  <div class="m-segment">
    <div class="segment_content">
      <!-- <a href="prek.php"><img src="assets/img/Prek_vektor_white.svg" alt="Prek" title="Prek" /><span>Prek</span></a>
      <a href="webdesign.php"><img src="assets/img/Prek_vektor_white.svg" alt="MiMove" title="MiMove" /><span>Design</span></a>
    </div>
  </div>
  -->
  <!-- <div class="m-segment">
    <div class="segment_content">
      <a href="webdesign.php"><img src="assets/img/logo-mimove-white.svg" alt="MiMove" title="MiMove" /><span>Webdesign</span></a>
    </div>
  </div> -->
</nav>

<div class="credits m-segment">
    <h2>Scripts used creating this site</h2>
    <a href="https://twitter.com/tayokoart">smoothState</a>
    <a href="https://github.com/miguel-perez/jquery.smoothState.js/graphs/contributors">juxtapose</a>
    <a href="https://github.com/miguel-perez/jquery.smoothState.js/blob/master/LICENSE.md">expandable image gallery</a>
    <a href="https://github.com/miguel-perez/jquery.smoothState.js/blob/master/LICENSE.md">headroom</a>
</div>
